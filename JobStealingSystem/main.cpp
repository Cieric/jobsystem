#include "JobSystem/Job.h"
#include "JobSystem/JobSystem.h"
#include "JobSystem/Splitters/DataSizeSplitter.h"
#include <iostream>
#include <chrono>

void setInt(int* data, size_t count)
{
	for (size_t i = 0; i < count; i++)
		data[i] += 1;
}

int main(int argc, char** argv) {
	
	{
		auto start = std::chrono::high_resolution_clock::now();
		volatile size_t blahSize = 134217727; //Marked as volitial to avoid compiler optimizations based on size
		int* blah = (int*)calloc(blahSize, sizeof(int));
		printf("blah: {%p, %p}\n", blah, blah + blahSize);
		for (uint64_t i = 0; i < blahSize; i++)
			blah[i] += 1;
		auto end = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		printf("time: %lldms\n", duration.count());
		free((void*)blah);
	}

	{
		auto start = std::chrono::high_resolution_clock::now();
		size_t blahSize = 134217727;
		int* blah = (int*)calloc(blahSize, sizeof(int));
		printf("blah: {%p, %p}\n", blah, blah+ blahSize);
		JobSystem::Init(8);
		Job* job = jobs::parallel_for(blah, blahSize, &setInt, DataSizeSplitter(32 * 1024));
		JobSystem::Run(0, job);
		JobSystem::Wait(0, job);
		auto end = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		printf("time: %lldms\n", duration.count());
		free(blah);
	}
	getchar();
	return 0;
}
