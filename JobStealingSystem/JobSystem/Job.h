#pragma once
#include <atomic>

struct Job;

typedef void(*JobFunction)(size_t, Job*, const void*);

struct Job
{
	char* debugInfo = nullptr;
	JobFunction function;
	Job* parent;
	std::atomic_intptr_t unfinishedJobs;
	void* data;
	//char padding[]; //I don't know what this value shou
};