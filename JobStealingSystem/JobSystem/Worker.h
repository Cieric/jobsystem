#pragma once
#include <thread>
#include "WorkStealingQueue.h"

class Worker
{
	size_t id;
	WorkStealingQueue jobs;
	std::thread workingThread;
public:
	Worker();
	void Work();
	WorkStealingQueue* GetWorkStealingQueue();
};
