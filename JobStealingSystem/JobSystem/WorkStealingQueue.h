#pragma once

struct Job;

class WorkStealingQueue
{
	long m_bottom = 0;
	long m_top = 0;
	Job** m_jobs = nullptr;
public:
	WorkStealingQueue();
	size_t Size();
	void Push(Job* job);
	Job* Pop();
	Job* Steal();
};