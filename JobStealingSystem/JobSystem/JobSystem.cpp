#include <atomic>
#include <vector>
#include "JobSystem.h"
#include "WorkStealingQueue.h"
#include "Worker.h"

size_t g_workerThreadCount;
WorkStealingQueue g_mainJobQueue;
std::vector<Worker*> workers;
Job g_jobAllocator[MAX_JOB_COUNT];
std::atomic_size_t g_allocatedJobs = 0u;

void JobSystem::Init(size_t threads)
{
	g_workerThreadCount = threads;
	for (size_t i = 0; i < threads - 1; i++)
	{
		Worker* worker = new Worker();
		workers.push_back(worker);
	}
}

WorkStealingQueue * JobSystem::GetWorkerThreadQueue(uint64_t threadid)
{
	WorkStealingQueue* stealQueue = &g_mainJobQueue;
	if (threadid > 0) stealQueue = workers[threadid - 1]->GetWorkStealingQueue();
	return stealQueue;
}

size_t JobSystem::GetWorkerID()
{
	static size_t id = 0;
	return id++;
}

bool JobSystem::IsEmptyJob(Job * job)
{
	return job == nullptr;
}

Job * JobSystem::StealJob(size_t threadid)
{
	unsigned int randomIndex = rand() % g_workerThreadCount;
	if (randomIndex == threadid)
		return nullptr;

	WorkStealingQueue* stealQueue = GetWorkerThreadQueue(threadid);
	Job* stolenJob = stealQueue->Steal();
	if (IsEmptyJob(stolenJob))
		return nullptr;
	return stolenJob;
}

Job * JobSystem::GetJob(size_t threadid)
{
	WorkStealingQueue* queue = GetWorkerThreadQueue(threadid);

	Job* job = queue->Pop();
	if (IsEmptyJob(job))
	{
		// this is not a valid job because our own queue is empty, so try stealing from some other queue
		unsigned int randomIndex = rand() % g_workerThreadCount;
		WorkStealingQueue* stealQueue = GetWorkerThreadQueue(threadid);
		if (randomIndex == threadid)
			return nullptr;

		Job* stolenJob = stealQueue->Steal();
		if (IsEmptyJob(stolenJob))
			return nullptr;

		return StealJob(threadid);
	}

	return job;
}

void JobSystem::Finish(Job * job)
{
	const intptr_t unfinishedJobs = --job->unfinishedJobs;

	if ((unfinishedJobs == 0) && (job->parent))
	{
		Finish(job->parent);
	}
}

void JobSystem::Run(size_t threadid, Job * job)
{
	WorkStealingQueue* queue = GetWorkerThreadQueue(threadid);
	queue->Push(job);
}

void JobSystem::Execute(size_t threadid, Job * job)
{
	(job->function)(threadid, job, job->data);
	Finish(job);
}

Job * JobSystem::AllocateJob()
{
	const size_t index = g_allocatedJobs++;
	//printf("Job #%I64u Allocated!\n", index);
	return &g_jobAllocator[index & (MAX_JOB_COUNT - 1u)];
}

Job * JobSystem::CreateJob(JobFunction function, void * data)
{
	Job* job = AllocateJob();
	job->function = function;
	job->data = data;
	job->parent = nullptr;
	job->unfinishedJobs = 1;
	return job;
}

Job * JobSystem::CreateJobAsChild(Job * parent, JobFunction function, void * data)
{
	parent->unfinishedJobs++;
	Job* job = AllocateJob();
	job->function = function;
	job->parent = parent;
	job->unfinishedJobs = 1;
	job->data = data;
	return job;
}

bool JobSystem::HasJobCompleted(const Job * job)
{
	return job->unfinishedJobs == 0;
}

void JobSystem::Wait(size_t threadid, const Job * job)
{
	while (!HasJobCompleted(job))
	{
		Job* nextJob = GetJob(threadid);
		if (nextJob)
		{
			Execute(threadid, nextJob);
		}
	}
}
