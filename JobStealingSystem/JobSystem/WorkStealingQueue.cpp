#include "WorkStealingQueue.h"
#include <memory>
#include <intrin.h>

static const unsigned int NUMBER_OF_JOBS = 4096u;
static const unsigned int MASK = NUMBER_OF_JOBS - 1u;

WorkStealingQueue::WorkStealingQueue()
{
	m_jobs = (Job**)malloc(sizeof(Job*) * NUMBER_OF_JOBS);
}

size_t WorkStealingQueue::Size()
{
	long t = m_top;
	_ReadBarrier();
	long b = m_bottom;
	return t - b;
}

void WorkStealingQueue::Push(Job* job)
{
	long b = m_bottom;
	m_jobs[b & MASK] = job;
	_ReadBarrier();
	m_bottom = b + 1;
}

Job * WorkStealingQueue::Pop(void)
{
	long b = m_bottom - 1;
	_InterlockedExchange(&m_bottom, b);

	long t = m_top;
	if (t <= b)
	{
		Job* job = m_jobs[b & MASK];
		if (t != b)
		{
			return job;
		}

		if (_InterlockedCompareExchange(&m_top, t + 1, t) != t)
			job = nullptr;

		m_bottom = t + 1;
		return job;
	}
	else
	{
		m_bottom = t;
		return nullptr;
	}
}

Job * WorkStealingQueue::Steal(void)
{
	long t = m_top;
	_ReadBarrier();
	long b = m_bottom;
	if (t < b)
	{
		Job* job = m_jobs[t & MASK];
		if (_InterlockedCompareExchange(&m_top, t + 1, t) != t)
			return nullptr;
		return job;
	}
	return nullptr;
}