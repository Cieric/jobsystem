#pragma once

class DataSizeSplitter
{
public:
	explicit DataSizeSplitter(size_t size)
		: m_size(size)
	{
	}

	template <typename T>
	inline bool Split(size_t count) const
	{
		return (count * sizeof(T) > m_size);
	}

private:
	size_t m_size;
};