#pragma once

class CountSplitter
{
public:
	explicit CountSplitter(size_t count)
		: m_count(count)
	{
	}

	template <typename T>
	inline bool Split(size_t count) const
	{
		return (count > m_count);
	}

private:
	size_t m_count;
};