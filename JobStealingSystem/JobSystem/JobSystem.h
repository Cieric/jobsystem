#pragma once
#include "Job.h"
#define MAX_JOB_COUNT 1048576

class Worker;
struct Job;
class WorkStealingQueue;

namespace JobSystem
{
	void Init(size_t threads);
	WorkStealingQueue * GetWorkerThreadQueue(uint64_t threadid);
	size_t GetWorkerID();
	bool IsEmptyJob(Job* job);
	Job * StealJob(size_t threadid);
	Job * GetJob(size_t threadid);
	void Finish(Job * job);
	void Run(size_t threadid, Job * job);
	void Execute(size_t threadid, Job * job);
	Job * AllocateJob();
	Job * CreateJob(JobFunction function, void * data);
	Job * CreateJobAsChild(Job * parent, JobFunction function, void * data);
	bool HasJobCompleted(const Job * job);
	void Wait(size_t threadid, const Job * job);
};

namespace jobs
{
	template <typename T, typename S>
	struct parallel_for_job_data
	{
		typedef T DataType;
		typedef S SplitterType;

		parallel_for_job_data(DataType* data, size_t count, void(*function)(DataType*, size_t), const SplitterType& splitter)
			: data(data)
			, count(count)
			, function(function)
			, splitter(splitter)
		{
		}

		DataType* data;
		size_t count;
		void(*function)(DataType*, size_t);
		SplitterType splitter;
	};

	template <typename JobData>
	void parallel_for_job(size_t threadid, Job* job, const void* jobData)
	{
		const JobData* data = static_cast<const JobData*>(jobData);
		const JobData::SplitterType& splitter = data->splitter;

		if (splitter.Split<JobData::DataType>(data->count))
		{
			const size_t leftCount = data->count / 2u;
			Job* left = JobSystem::CreateJobAsChild(job, &jobs::parallel_for_job<JobData>, new JobData(data->data, leftCount, data->function, splitter));
			JobSystem::Run(threadid, left);

			const size_t rightCount = data->count - leftCount;
			Job* right = JobSystem::CreateJobAsChild(job, &jobs::parallel_for_job<JobData>, new JobData(data->data + leftCount, rightCount, data->function, splitter));
			JobSystem::Run(threadid, right);
		}
		else
		{
			(data->function)(data->data, data->count);
		}
	}

	template <typename T, typename S>
	Job* parallel_for(T* data, size_t count, void(*function)(T*, size_t), const S& splitter)
	{
		typedef parallel_for_job_data<T, S> JobData;
		Job* job = JobSystem::CreateJob(&jobs::parallel_for_job<JobData>, new JobData(data, count, function, splitter));
		return job;
	}
}