#include "Job.h"
#include "JobSystem.h"
#include <intrin.h>
#include <random>

Worker::Worker()
{
	id = JobSystem::GetWorkerID();
	workingThread = std::thread(&Worker::Work, this);
}

void Worker::Work()
{
	while (true)
	{
		Job* job = (jobs.Size() > 0) ? jobs.Pop() : JobSystem::StealJob(id);
		if (job == nullptr)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			continue;
		}
		JobSystem::Execute(id, job);
	}
}

WorkStealingQueue * Worker::GetWorkStealingQueue()
{
	return &jobs;
}